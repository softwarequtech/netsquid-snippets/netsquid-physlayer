API Documentation
-----------------

Below are the modules of this package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/absorption_connection.rst
   modules/classical_connection.rst
   modules/classical_errors.rst
   modules/detectors.rst
   modules/heralded_connection.rst
   modules/pair_preparation.rst
   modules/photon_noise.rst
   modules/quantum_program_library.rst
