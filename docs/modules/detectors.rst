detectors
---------

Module containing various detectors components.

.. automodule:: netsquid_physlayer.detectors
    :members:
