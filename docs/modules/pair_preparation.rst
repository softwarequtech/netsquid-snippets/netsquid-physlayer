pair_preparation
----------------

Module containing simple factories for producing certain entangled states.

.. automodule:: netsquid_physlayer.pair_preparation
    :members:
