quantum_program_library
-----------------------

Module containing functions to generate certain quantum programs.

.. automodule:: netsquid_physlayer.quantum_program_library
    :members:
