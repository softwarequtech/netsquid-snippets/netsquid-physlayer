classical_errors
----------------

Module containing various classical error models.

.. automodule:: netsquid_physlayer.classical_errors
    :members:
