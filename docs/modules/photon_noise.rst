photon_noise
------------

Module containing base-classes for photon noise.

.. automodule:: netsquid_physlayer.photon_noise
    :members:
